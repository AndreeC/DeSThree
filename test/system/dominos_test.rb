require "application_system_test_case"

class DominosTest < ApplicationSystemTestCase
  setup do
    @domino = dominos(:one)
  end

  test "visiting the index" do
    visit dominos_url
    assert_selector "h1", text: "Dominos"
  end

  test "creating a Domino" do
    visit dominos_url
    click_on "New Domino"

    fill_in "Answer1", with: @domino.answer1
    fill_in "Answer2", with: @domino.answer2
    fill_in "Instruction", with: @domino.instruction
    fill_in "Question", with: @domino.question
    click_on "Create Domino"

    assert_text "Domino was successfully created"
    click_on "Back"
  end

  test "updating a Domino" do
    visit dominos_url
    click_on "Edit", match: :first

    fill_in "Answer1", with: @domino.answer1
    fill_in "Answer2", with: @domino.answer2
    fill_in "Instruction", with: @domino.instruction
    fill_in "Question", with: @domino.question
    click_on "Update Domino"

    assert_text "Domino was successfully updated"
    click_on "Back"
  end

  test "destroying a Domino" do
    visit dominos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Domino was successfully destroyed"
  end
end
