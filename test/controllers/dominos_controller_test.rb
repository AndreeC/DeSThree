require 'test_helper'

class DominosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @domino = dominos(:one)
  end

  test "should get index" do
    get dominos_url
    assert_response :success
  end

  test "should get new" do
    get new_domino_url
    assert_response :success
  end

  test "should create domino" do
    assert_difference('Domino.count') do
      post dominos_url, params: { domino: { answer1: @domino.answer1, answer2: @domino.answer2, instruction: @domino.instruction, question: @domino.question } }
    end

    assert_redirected_to domino_url(Domino.last)
  end

  test "should show domino" do
    get domino_url(@domino)
    assert_response :success
  end

  test "should get edit" do
    get edit_domino_url(@domino)
    assert_response :success
  end

  test "should update domino" do
    patch domino_url(@domino), params: { domino: { answer1: @domino.answer1, answer2: @domino.answer2, instruction: @domino.instruction, question: @domino.question } }
    assert_redirected_to domino_url(@domino)
  end

  test "should destroy domino" do
    assert_difference('Domino.count', -1) do
      delete domino_url(@domino)
    end

    assert_redirected_to dominos_url
  end
end
