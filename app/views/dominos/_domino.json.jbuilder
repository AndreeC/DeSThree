json.extract! domino, :id, :instruction, :question, :answer1, :answer2, :created_at, :updated_at
json.url domino_url(domino, format: :json)
