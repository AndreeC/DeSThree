json.extract! test_answer, :id, :test, :question, :answer1, :answer2, :created_at, :updated_at
json.url test_answer_url(test_answer, format: :json)
