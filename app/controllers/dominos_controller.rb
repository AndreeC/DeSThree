class DominosController < ApplicationController
  before_action :set_domino, only: [:show, :edit, :update, :destroy]

  # GET /dominos
  # GET /dominos.json
  def index
    @dominos = Domino.all
  end

  # GET /dominos/1
  # GET /dominos/1.json
  def show
  end

  # GET /dominos/new
  def new
    @domino = Domino.new
  end

  # GET /dominos/1/edit
  def edit
  end

  # POST /dominos
  # POST /dominos.json
  def create
    @domino = Domino.new(domino_params)

    respond_to do |format|
      if @domino.save
        format.html { redirect_to @domino, notice: 'Domino was successfully created.' }
        format.json { render :show, status: :created, location: @domino }
      else
        format.html { render :new }
        format.json { render json: @domino.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /dominos/1
  # PATCH/PUT /dominos/1.json
  def update
    respond_to do |format|
      if @domino.update(domino_params)
        format.html { redirect_to @domino, notice: 'Domino was successfully updated.' }
        format.json { render :show, status: :ok, location: @domino }
      else
        format.html { render :edit }
        format.json { render json: @domino.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /dominos/1
  # DELETE /dominos/1.json
  def destroy
    @domino.destroy
    respond_to do |format|
      format.html { redirect_to dominos_url, notice: 'Domino was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_domino
      @domino = Domino.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def domino_params
      params.require(:domino).permit(:instruction, :question, :answer1, :answer2)
    end
end
