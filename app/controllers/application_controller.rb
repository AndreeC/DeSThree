class ApplicationController < ActionController::Base
    before_action :load_domino


    def load_domino
        @dominos = Domino.all
    end
end
