class CreateDominos < ActiveRecord::Migration[5.2]
  def change
    create_table :dominos do |t|
      t.text :instruction
      t.string :question
      t.integer :answer1
      t.integer :answer2
      t.timestamps

    end
  end
end
