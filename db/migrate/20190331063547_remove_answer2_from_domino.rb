class RemoveAnswer2FromDomino < ActiveRecord::Migration[5.2]
  def change
    remove_column :dominos, :answer2, :integer
  end
end
