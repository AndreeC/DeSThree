class RemoveAnswer1FromDomino < ActiveRecord::Migration[5.2]
  def change
    remove_column :dominos, :answer1, :integer
  end
end
