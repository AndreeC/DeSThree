class CreateTestAnswers < ActiveRecord::Migration[5.2]
  def change
    create_table :test_answers do |t|
      t.string :test
      t.integer :question
      t.string :answer1
      t.string :answer2

      t.timestamps
    end
  end
end
